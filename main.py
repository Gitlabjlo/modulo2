import bd
from modelos import Panamenismos

def run():
    try:
        Palabra1 = Panamenismos('Que sopa', 'Como estas?')
        bd.session.add(Palabra1)
        bd.session.commit()
        Palabra2 = Panamenismos('Pollo(a)', 'Novio/Novia')
        bd.session.add(Palabra2)
        bd.session.commit()
        Palabra3 = Panamenismos('Pelo el bollo', 'Se murio')
        bd.session.add(Palabra3)
        bd.session.commit()
        print("Insercion exitosa")
    except Error:
       print("Error en insercion")
      


    import os
    def menu():
        #os.system("cls")
        print ()
        print ()
        print ("Jerga Panamenas")
        print ("palabras que se usan en Panamá mas que en otros países en la vida cotidiana.")
        print ("Selecciona una opcion del siguiente menu:")
        print ("1) AGREGA UNA NUEVA PALABRA")
        print ("2) EDITA UNA PALABRA EXISTENTE")
        print ("3) ELIMINA UNA PALABRA EXISTENTE")
        print ("4) VER LISTADO DE PALABRAS")
        print ("5) BUSCAR SIGNIFICADO DE PALABRA")
        print ("6) SALIR")
        print ()
        

    while True:
        menu()

    # Leemos lo que ingresa el usuario
        opcion = input("Selecciona :")
        print ()
        if opcion == "1":
            print ("Seleccionaste la opcion que agrega una palabra nueva")
            palabra = input("Palabra: ")
            significado = input("Signidicado: ")
            x=Panamenismos(palabra, significado)
            bd.session.add(x)
            bd.session.commit()
            print(x.id)
            
        elif opcion == "2":
            print ("Seleccionaste la opcion que modifica una Palabra")
            id = int (input ("Ingresa un ID: "))
            palabra = input ("Ingresa la palabra actualizada: ")
            x=bd.session.query(Panamenismos).filter(Panamenismos.id == id).first()
            x.palabra = palabra
            bd.session.add(x)
            bd.session.commit()
            print(x.id)
            
        elif opcion == "3":
            print ("Seleccionaste la opcion que borra una palabra")
            id = int (input ("Ingresa el ID de la palabra que deseas eliminar: ")) 
            bd.session.query(Panamenismos).filter(Panamenismos.id == id).delete()
            bd.session.commit()
            
                       
        elif opcion == "4":
            print ("Seleccionaste la opcion que nos muestra todos las palabras en la tabla")
            print ("")
            palabraspan = bd.session.query(Panamenismos).all()
            print(palabraspan)
                                
        elif opcion == "5":
            print ("Seleccionaste la opcion que nos busca el significado de la palabra")
            palabra = input ("Ingresa la palabra que quieres saber su significado: ")
            x=bd.session.query(Panamenismos).filter(Panamenismos.palabra==palabra).first()
            print(x.significado)
            
        elif opcion == "6":
            print ("Si quieres aprender mas Panamanismos vuelve pronto")
            exit()
        else:
            print("opcion invalida")
   
    

if __name__ == '__main__':
    bd.Base.metadata.create_all(bd.engine)
    run()
