import bd
#import mysql.connector as mariadb
#import mariadb

from sqlalchemy import Column, Integer, String, String

class Panamenismos(bd.Base):
    __tablename__ = 'panamenismos'

    id = Column(Integer, primary_key=True)
    palabra = Column(String(30), nullable=False)
    significado = Column(String(100), nullable=False)

    def __init__(self, palabra, significado):
        self.palabra = palabra
        self.significado = significado

    def __repr__(self):
        return f'Panamenismos({self.palabra}, {self.significado})'

    def __str__(self):
        return self.palabra
