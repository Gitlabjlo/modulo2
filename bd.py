from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

##funcionan las dos conexiones
#engine = create_engine('sqlite:///Jergapana.db')
engine = create_engine('mariadb+pymysql://root@localhost/Jergapana')
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()
